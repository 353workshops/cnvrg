# Go Workshop @ cnvrg

Miki Tebeka
📬 [miki@353solutions.com.com](mailto:miki@353solutions.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [LinkedIn Learning Classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Books](https://pragprog.com/search/?q=miki+tebeka)

[Syllabus](_extra/syllabus.pdf)
[Final exercise](_extra/dld.md)

---

## Day 1

### Agenda

- Strings & formatted output
	- What’s a string?
	- Unicode basics
	- Using the “fmt” package for formatted output
- Calling REST APIs
	- Making HTTP calls with net/http
	- Defining structs
	- Serializing JSON
- Serialization (JSON, protocol buffers)
- Working with files
	- Handling errors
	- Using defer to manage resources
	- Working with io.Reader & io.Writer interfaces
- Sorting
	- Working with slices
	- Writing methods
	- Understanding interfaces
- Catching panics
	- The built-in recover function
	- Named return values
- Distributing work
	- Using goroutines & channels
	- Using the sync package to coordinate work
- Timeouts & cancellations
	- Working with multiple channels using select
	- Using context for timeouts & cancellations
	- Standard library support for context

### Code

- [banner.go](banner/banner.go) - Strings & Unicode
- [github.go](github/github.go) - Calling REST APIs, JSON, Protocol Buffers
- [sha1.go](sha1/sha1.go) - Working with files, `defer`, error handling, `io.Reader` & `io.Writer`
- [slices.go](slices/slices.go) - Working with slices
- [driver.go](driver/driver.go) - Embedding is not inheritance
- [game.go](game/game.go) - Structs, methods & interfaces
- [div.go](div/div.go) - Handling panics
- [go_chan.go](go_chan/go_chan.go) - Goroutines & channels
- [rtb.go](rtb/rtb.go) - Using context for timeouts & cancellations
- [counter.go](counter/counter.go) - The race detector, `sync.Mutex` and `sync/atomic`


### Exercises

#### Player

- Add a field called `Keys` to player which is a slice of `Key`
- Implement `func (p *Player) Found(k Key)`, it should add `k` to keys only if it's not there already

#### Concurrency

- Download an extract [taxi.tar](https://storage.googleapis.com/353solutions/c/data/taxi.tar)
- Run [taxi.go](taxi/taxi.go) and measure how much time it took (error is expected)
- Change the code to work with a goroutine per file
- Measure the time again - what is your speedup?

### Links

- [Effective Go](https://go.dev/doc/effective_go)
- [Go proverbs](https://go-proverbs.github.io/)
- Unicode
    - [Plain Text](https://www.youtube.com/watch?v=4mRxIgu9R70&t=11s)
    - [golang.org/x/text](https://pkg.go.dev/golang.org/x/text?tab=doc)
    - [Unicode Table](https://unicode-table.com/en/)
    - [Arrays, slices & strings](https://blog.golang.org/slices)
    - [Strings, bytes, runes and characters in Go](https://blog.golang.org/strings)
- [stringer](https://pkg.go.dev/golang.org/x/tools/cmd/stringer)
    - `go install golang.org/x/tools/cmd/stringer@latest`
    - `stringer -type Key` (in `game`)
        - In your bashrc: `export PATH="$(go env GOPATH)/bin:${PATH}"`
- JSON
    - [encoding/json](https://golang.org/pkg/encoding/json/) - Use this
    - [Custom marshalling](https://golang.org/pkg/encoding/json/#example__customMarshalJSON)
    - [Using anonymous nested structs](https://pythonwise.blogspot.com/2020/01/nested-structs-for-http-calls.html)
    - [mapstructure](https://github.com/mitchellh/mapstructure) - Decode `map[string]any` to a struct
- Protocol Buffers
    - [Protocol Buffer Basics: Go](https://developers.google.com/protocol-buffers/docs/gotutorial)
    - [google.golang.org/protobuf/proto](https://pkg.go.dev/google.golang.org/protobuf/proto?tab=doc)
    - [A new Go API for Protocol Buffers](https://blog.golang.org/protobuf-apiv2)
    - [protoc Download](https://github.com/protocolbuffers/protobuf/releases/)
    - [gRPC](https://grpc.io/) - Protocol Buffers over HTTP2
    - [Buf](https://buf.build/) - Protocol Buffers tools
    - [protojson](https://pkg.go.dev/google.golang.org/protobuf/encoding/protojson) Marshal protobuf generate objects to JSON the right way (don't use `encoding/json` on them)
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- [Garbage Collection in Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
- [Slice Tricks Cheatsheet](https://ueokande.github.io/go-slice-tricks/)
- [Go Slices: usage and internals](https://go.dev/blog/slices-intro)
- [Methods, interfaces & embedded types in Go](https://www.ardanlabs.com/blog/2014/05/methods-interfaces-and-embedded-types.html)
- [Methods & Interfaces](https://go.dev/tour/methods/1) in the Go tour
- [sort examples](https://pkg.go.dev/sort/#pkg-examples) - Read and try to understand
- [Generics can make your Go code slower](https://planetscale.com/blog/generics-can-make-your-go-code-slower)
- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [The race detector](https://go.dev/doc/articles/race_detector)
- [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup)
- [Go Concurrency Patterns: Pipelines and cancellation](https://go.dev/blog/pipelines)
- [Go Concurrency Patterns: Context](https://go.dev/blog/context)
- [Curious Channels](https://dave.cheney.net/2013/04/30/curious-channels)
- [The Behavior of Channels](https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html)
- [Channel Semantics](https://www.353solutions.com/channel-semantics)
- [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
- [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law) - Limits of concurrency
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
- [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html)
- [Method sets](https://www.youtube.com/watch?v=Z5cvLOrWlLM) video
- Closing HTTP response body
    - The [docs](https://pkg.go.dev/net/http) say: "The caller must close the response body when finished with it"
    - [TIL: Go Response Body MUST be closed, even if you don’t read it](https://manishrjain.com/must-close-golang-http-response)
    - [Is it necessary to close the Body in the http.Response object in golang?](https://andrii-kushch.medium.com/is-it-necessary-to-close-the-body-in-the-http-response-object-in-golang-171c44c9394d)
- [stringer](https://pkg.go.dev/golang.org/x/tools/cmd/stringer) - Generate `fmt.Stringer` for your types

### Data & Other

- [http.log.gz](_extra/http.log.gz)
- [Slices](_extra/slices.png)
- [Error cost](_extra/cost.png)

## Day 2

### Agenda

- Testing your code
	- Working with the testing package
	- Using testify
	- Managing dependencies with go mod
- Structuring your code
	- Writing sub-packages
- HTTP servers
	- Writing middleware
	- Routing
	- Testing handlers
	- Working with SQL
- Working with gRPC
	- Writing definition file
	- Using protocol buffers
	- Servers & Clients
- Adding metrics & logging
	- Using expvar for metrics
	- Using the log package and a look at user/zap
- Configuration patterns
	- Reading environment variables and taking a look at external packages
	- Using the flag package for command line processing
- Building docker images
	- Multi stage builds
	- Embedding version in the executable

### Code

[nlp](nlp) project

### Links

- [Linus' Law](https://en.wikipedia.org/wiki/Linus%27s_law)
- [Hyrum's Law](https://www.hyrumslaw.com/)
- [The Complete Guide to HTTP Timeouts](https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts)
- [How I Write HTTP Services in Go After 13 years](https://grafana.com/blog/2024/02/09/how-i-write-http-services-in-go-after-13-years)
- Databases
    - [database/sql](https://pkg.go.dev/database/sql)
    - [sqlx](https://jmoiron.github.io/sqlx) - Easier SQL to struct
    - [sqlc](https://docs.sqlc.dev) - Compile SQL to Go
    - [gorm](https://gorm.io/) - ORM
- [goreleaser](https://goreleaser.com)
    - Has a [github action](https://github.com/goreleaser/goreleaser-action)
- [Conway's Law](https://martinfowler.com/bliki/ConwaysLaw.html)
- [The Twelve-Factor App](https://12factor.net)
- Command Line & Configuration
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
    - [viper](https://github.com/spf13/viper) & [cobra](https://github.com/spf13/cobra)
    - [cli](https://cli.urfave.org/)
- Logging 
    - Built-in [slog](https://pkg.go.dev/log/slog) - Go 1.21+
    - [uber/zap](https://pkg.go.dev/go.uber.org/zap)
    - [logrus](https://github.com/sirupsen/logrus)
- Metrics
    - Built-in [expvar](https://pkg.go.dev/expvar/)
    - [Open Telemetry](https://opentelemetry.io/)
    - [Prometheus](https://pkg.go.dev/github.com/prometheus/client_golang/prometheus)
- [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
- Code Layout
    - [Organizing a Go Module](https://go.dev/doc/modules/layout)
    - [Tutorial: Getting started with multi-module workspaces](https://go.dev/doc/tutorial/workspaces)
    - [ArdanLabs service repo](https://github.com/ardanlabs/service)
    - [How to Write Go Code](https://go.dev/doc/code.html)
- Documentation
    - [Godoc: documenting Go code](https://go.dev/blog/godoc)
    - [Go Doc Comments](https://go.dev/doc/comment)
    - [Testable examples in Go](https://go.dev/blog/examples)
    - [Go documentation tricks](https://godoc.org/github.com/fluhus/godoc-tricks)
    - [gob/doc.go](https://github.com/golang/go/blob/master/src/encoding/gob/doc.go) of the `gob` package. Generates [this documentation](https://pkg.go.dev/encoding/gob/)
    - `go install golang.org/x/pkgsite/cmd/pkgsite@latest` (require go 1.18+)
    - `pkgsite -http=:8080` (open browser on http://localhost:8080/${module name})
- [Out Software Dependency Problem](https://research.swtch.com/deps) - Good read on dependencies by Russ Cox
- Linters (static analysis)
    - [staticcheck](https://staticcheck.io/)
    - [golangci-lint](https://golangci-lint.run/)
    - [gosec](https://github.com/securego/gosec) - Security oriented
    - [vulncheck](https://pkg.go.dev/golang.org/x/vuln/vulncheck) - Check for CVEs
    - [golang.org/x/tools/go/analysis](https://pkg.go.dev/golang.org/x/tools/go/analysis) - Helpers to write analysis tools (see [example](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/))
- Testing
    - [testing](https://pkg.go.dev/testing/)
    - [testify](https://pkg.go.dev/github.com/stretchr/testify) - Many test utilities (including suites & mocking)
    - [Ginkgo](https://onsi.github.io/ginkgo/)
    - [Tutorial: Getting started with fuzzing](https://go.dev/doc/tutorial/fuzz)
        - [testing/quick](https://pkg.go.dev/testing/quick) - Initial fuzzing library
    - [test containers](https://golang.testcontainers.org/quickstart/)
- HTTP Servers
    - [net/http](https://pkg.go.dev/net/http/)
    - [net/http/httptest](https://pkg.go.dev/net/http/httptest)
    - [chi](https://github.com/go-chi/chi) - A nice web framework
- gRPC
    - [Go protobuf docs](https://pkg.go.dev/google.golang.org/protobuf)
    - [gRPC in Go](https://grpc.io/docs/languages/go/quickstart/)
    - [Error codes](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)
    - [metadata](https://grpc.io/docs/guides/metadata/)
    - [gRPCurl](https://github.com/fullstorydev/grpcurl)
        - [More gRPC tools](https://github.com/grpc-ecosystem/awesome-grpc#tools)
    - [go-grpc-middleware](https://github.com/grpc-ecosystem/go-grpc-middleware)
    - [grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway)
    - [buf](https://buf.build/)
    - [connect](https://pkg.go.dev/connectrpc.com/connect)

### Data & Other

- [tokenize_cases.yml](_extra/tokenize_cases.yml)
- `go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28`
- `go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2`
- `export PATH="$PATH:$(go env GOPATH)/bin`
- `grpcurl --plaintext localhost:8081 list`
- `grpcurl --plaintext localhost:8081 list NLP`
- `grpcurl --plaintext localhost:8081 describe NLP.Tokenize`
- `grpcurl --plaintext -d @ localhost:8081  NLP.Tokenize < ./testdata/tok-req.json`
