package main

import (
	"fmt"
	"strings"
	"unicode/utf8"

	"go.uber.org/zap"
)

func main() {
	banner("Go", 6)
	//   Go
	// ------
	banner("G☺", 6)

	s := "G☺!"
	fmt.Println(len(s))      // in bytes
	fmt.Printf("%c\n", s[2]) // byte

	for i, c := range s {
		fmt.Printf("%d: %c\n", i, c)
	}

	rs := []rune(s)
	fmt.Println("rune count:", len(rs))
	for i, r := range rs {
		fmt.Printf("%d: %c\n", i, r)
	}

	// type byte = uint8
	// type rune = int32
	// s[0] = 'g' // strings are immutable

	n := 42
	fmt.Printf("%04d\n", n)

	a, b := 1, "1"
	fmt.Printf("a=%v, b=%v\n", a, b)
	fmt.Printf("a=%#v, b=%#v\n", a, b)

	log, err := zap.NewDevelopment()
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}
	log.Info("ab", zap.Int("a", a), zap.String("b", b))

	data := []byte(s) // type conversion
	data[0] = 'g'
	fmt.Println(s) // No change
}

func banner(text string, width int) {
	// offset := (width - len(text)) / 2 // BUG
	offset := (width - utf8.RuneCountInString(text)) / 2
	fmt.Print(strings.Repeat(" ", offset))
	fmt.Println(text)

	fmt.Println(strings.Repeat("-", width))
}
