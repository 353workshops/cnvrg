package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

var (
	// mu      sync.Mutex
	//counter int
	counter int64
)

func main() {
	var wg sync.WaitGroup

	for n := 0; n < 10; n++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < 1000; i++ {
				/*
					mu.Lock()
					{
					counter++
					}
						mu.Unlock()
				*/
				atomic.AddInt64(&counter, 1)
				time.Sleep(time.Microsecond)
			}
		}()
	}
	wg.Wait()
	fmt.Println(counter)
}

// go run -race
// also build, test
// Common practice: use -race in tests
