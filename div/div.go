package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))

	fmt.Println("main done")
}

// 3rd party function, I can't change it
func div(a, b int) int {
	if b == 0 {
		panic("division by zero")
	}

	return a / b
}

func safeDiv(a, b int) (result int) {
	// result is a variable here (like a & b)
	defer func() {
		// err will be non-nil only if there's a panic
		if err := recover(); err != nil {
			fmt.Printf("div error: %s\n", err)
			result = math.MaxInt64
		}
	}()

	return div(a, b)
}
