package main

import (
	"fmt"
	"time"
)

func main() {
	var i Item
	fmt.Println("i:", i)

	i = Item{10, 20} // by position, must pass all values
	fmt.Println("i:", i)

	i = Item{Y: 100} //, X: 200} // by name, any order,
	fmt.Println("i:", i)

	fmt.Println(NewItem(10, 20))
	fmt.Println(NewItem(10, 2000))

	i2, err := NewItem(10, 20)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	i2.Move(300, 400)
	fmt.Println("i2 (move):", i2)

	t := time.Now()
	t = t.Add(time.Hour)
	fmt.Println("hour from now:", t)

	fmt.Println("i:", i)
	i.Move(500, 900)
	fmt.Println("i (move):", i)

	p1 := Player{
		Name: "Parzival",
		Item: Item{100, 200},
	}
	fmt.Printf("p1: %s at %d/%d\n", p1.Name, p1.X, p1.Y)
	fmt.Println("p1.Item.X:", p1.Item.X)

	p1.Move(120, 340)
	fmt.Printf("p1: %s at %d/%d\n", p1.Name, p1.X, p1.Y)

	ms := []mover{
		&i,
		i2,
		&p1,
	}
	moveAll(ms, 99, 88)
	for _, m := range ms {
		fmt.Println(m)
	}

	k := Jade
	fmt.Println("k:", k)
}

// fmt.Stringer
// See also golang.org/x/tools/cmd/stringer
func (k Key) String() string {
	switch k {
	case Copper:
		return "copper"
	case Jade:
		return "jade"
	case Crystal:
		return "crystal"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

const (
	Copper Key = iota + 1
	Jade
	Crystal
)

func moveAll(ms []mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

/*
Though experiment: sorting

type Sortable interface {
	func Less(i, j int) bool
	func Swap(i, j int)
	Len() int
}

func Sort(s Sortable) {
	//...
}
*/

/* Interfaces
- Say what you need,  not what you provide
- Small (stdlib avg < 2), >=5 fail code review
- Accept interface, return types

- Usages:
	- Polymorphism
	- How stdlib treats your types
		- fmt.Stringer
		- json.Marshaler, json.Unmarshaler
	- Mocking
*/

type mover interface {
	Move(int, int)
}

// Extending vs Embedding in OO

type Player struct {
	Name string
	Item // Player embeds Item
}

/* 3 "levels" of types
built-in: int, float64, string ...  -> value semantics
internal: slice, map, channel -> value semantics, but they are pointers
user defined: depends on underlying type
*/

// Move is a method, "i" is called "the receiver"
// i is a "pointer receiver"
func (i *Item) Move(x, y int) {
	i.X = x
	i.Y = y
}

/* Pointer vs value semantics
value semantics: each has its own copy
pointer semantics: same value for all, synchronization issue

Try to use the same semantics throughout

- Try to use value semantics as much as you can
- If you have mutation methods - use pointer
- If you have locks (sync.Mutex) - use pointer
- Unmarshaling - use pointer
*/

/*
func NewItem(x, y int) *Item
func NewItem(x, y int) Item
func NewItem(x, y int) (Item, error) {
*/
func NewItem(x, y int) (*Item, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("%d/%d out of range %d/%d", x, y, maxX, maxY)
	}

	i := Item{
		X: x,
		Y: y,
	}
	// ...
	return &i, nil // Go does escape analysis and will allocate i on the heap
}

const (
	maxX = 600
	maxY = 400
)

// Item is an item in the game.
type Item struct {
	X int
	Y int
}
