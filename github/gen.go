package main

//go:generate protoc --go_out pb --go_opt=paths=source_relative github.proto

// One time
// install protoc (brew, apt, choco ...)
// go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
// export PATH="$(go env GOPATH)/bin:${PATH}""
