package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {
	/*
		resp, err := http.Get("https://api.github.com/users/cnvrg")
	*/
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()

	url := "https://api.github.com/users/cnvrg"
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Println("ERROR:", resp.Status)
		return
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Printf("WARNING: can't close - %s", err)
		}
	}()
	// io.TeeReader

	fmt.Println("ctype:", resp.Header.Get("Content-Type"))
	// io.Copy(os.Stdout, resp.Body)

	// var r Reply
	var r struct { // anonymous struct
		Name     string
		NumRepos int `json:"public_repos"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	fmt.Printf("got: %#v\n", r)
}

// Types: API | Business | Storage

/* Zero vs Missing: If I get 0, did someone send 0 or did not send at all?
Solutions:
- Don't care
- Initialize with default values
- Use map[string]any (hard to work with, see mapstructure)
- Use pointers (only at API level, error prone, ugly)
*/

/*
type Reply struct {
	Name     string
	NumRepos int `json:"public_repos"` // field tag
}
*/

/* encoding/json will
- Fill only exported fields
- Have a name heuristic
- Will ignore unknown JSON fields
- Will ignore unknown struct fields
	- Go initializes to zero value
*/

/* encoding/json API
JSON -> io.Reader -> Go: json.NewDecoder
JSON -> []byte -> Go: json.Unmarshal
Go -> io.Writer -> JSON: json.NewEncoder
Go -> []byte -> JSON: json.Marshal
*/

/* JSON <-> Go types
string <-> string
bool <-> bool
null <-> nil
number <-> float64, float32, int, int8, ..., int64, uint, ..., uint64, complex, big.Int
array <-> []T, []any
object <-> struct, map[string]any

Go time.Time -> strings in RFC3339
Go []byte -> base64 encoded string

MIA
Timestamp <-> time.Time
comments
*/
