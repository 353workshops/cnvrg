module go-workshop

go 1.22.0

require (
	go.uber.org/zap v1.27.0
	google.golang.org/protobuf v1.32.0
)

require go.uber.org/multierr v1.10.0 // indirect
