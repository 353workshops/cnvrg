package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"
)

func main() {
	go fmt.Println("gorouitine")
	fmt.Println("main")

	for i := 0; i < 4; i++ {
		/* Fix 2: Loop local variable */
		i := i // "i" shadows "i" from line 12
		go func() {
			fmt.Println(i)
		}()
		/* Fix 1: Pass a parameter
		go func(n int) {
			fmt.Println(n)
		}(i)
		*/
		/*
			go func() {
				fmt.Println(i) // BUG in Go < 1.22, all goroutines use the same i
			}()
		*/
	}

	// main won't wait for goroutines
	time.Sleep(100 * time.Millisecond)

	// i++ // at least 3 machine instruction (read, modify, write)

	ch := make(chan string)
	go func() {
		ch <- "hi" // send
	}()
	msg := <-ch // receive
	fmt.Println("got:", msg)

	go func() {
		defer close(ch)
		for i := range 4 {
			ch <- fmt.Sprintf("message #%d", i)
		}
	}()

	for msg := range ch {
		fmt.Println("got:", msg)
	}

	/* What the above for loop does
	for {
		msg, ok := <- ch
		if !ok {
			break
		}
		fmt.Println("got:", msg)
	}
	*/

	msg = <-ch // ch is closed
	fmt.Printf("got (closed): %q\n", msg)

	// comma ok
	msg, ok := <-ch
	fmt.Printf("msg: %q, ok: %v\n", msg, ok)

	urls := []string{
		"https://cnvrg.io",
		"https://go.dev",
		"https://example.com/not-found",
	}
	start := time.Now()
	var wg sync.WaitGroup

	wg.Add(len(urls))
	// fan out pattern
	for _, url := range urls {
		// wg.Add(1)
		url := url
		go func() {
			defer wg.Done()
			urlStatus(url)
		}()
	}
	wg.Wait()
	fmt.Printf("%d urls in %v\n", len(urls), time.Since(start))

	// buffered channel to avoid goroutine leak
	ch1, ch2 := make(chan int, 1), make(chan int, 1)
	go func() {
		time.Sleep(500 * time.Millisecond)
		ch1 <- 1
	}()
	go func() {
		time.Sleep(100 * time.Millisecond)
		ch2 <- 2
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Println("ch1:", val)
	case val := <-ch2:
		fmt.Println("ch2:", val)
	case <-ctx.Done():
		fmt.Println("timeout")
	}
}

func urlStatus(url string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("error: %q - %s", url, err)
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Printf("error: %q - %s", url, resp.Status)
		return
	}

	log.Printf("%q - OK", url)
}

/* Channel semantics:
- send/receive will block until opposite operation
	- guarantee of delivery
	- buffered channel of capacity "n" has "n" non blocking sends
- receive from a closed channel will return zero value without blocking
- send to a closed chanel will panic
- closing closed chanel will panic
- send/receive to a nil channel will block forever
*/

/*
Deadlock detector:
	- If it found a deadlock - you have one
	- It it didn't find one - you probably have one

Concurrency: Dealing with several things at the same time.
Parallelism: Doing several things at the same time.

CPU bound: Image processing, digital signature, crypto, ...
IO Bound: Network, read from file, databases access

Goroutine context switch:
- go keyword
- I/O
- System call
- Time

Coordination: Wait/signal goroutines (channel, sync.WaitGroup)
Synchronization: Access to shared resource (sync.Mutex, ...)
*/
