package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"

	"github.com/353solutions/nlp"
	"github.com/google/uuid"
)

func main() {
	/* Go < 1.22
	/health - exact match
	/users/ - prefix match (sub tree)

	Go >= 1.22
	GET /health
	POST /users/{id}
	*/
	api := API{
		log: slog.Default().With("app", "nlp"),
	}
	mux := buildMux(&api)

	addr := ":8080"
	api.log.Info("server starting", "address", addr)
	srv := http.Server{
		Handler:           mux,
		Addr:              addr,
		ReadTimeout:       2 * time.Second,
		WriteTimeout:      time.Second,
		ReadHeaderTimeout: time.Second,
		IdleTimeout:       5 * time.Second,
	}
	if err := srv.ListenAndServe(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s", err)
		os.Exit(1)
	}
}

func topMiddleware(log *slog.Logger, h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// Pre handler
		start := time.Now()

		login, _, ok := r.BasicAuth() // JWT, ...
		// TODO: Authenticate user
		log.Info("user", "login", login, "ok", ok)

		vals := ctxValues{
			User: login,
			ID:   uuid.NewString(),
		}
		ctx := context.WithValue(r.Context(), ctxKey, &vals)
		r = r.WithContext(ctx)

		h.ServeHTTP(w, r)

		if vals.Err != nil {
			log.Error("downstream error", "error", vals.Err)
			// ...
		}

		// Post handler
		log.Info("request done", "path", r.URL.Path, "duration", time.Since(start))
	}

	return http.HandlerFunc(fn)
}

func getCtxValues(ctx context.Context) *ctxValues {
	vals, ok := ctx.Value(ctxKey).(*ctxValues)
	if !ok {
		return &ctxValues{
			User: "<unknown>",
			ID:   "XXX",
		}
	}

	return vals
}

var ctxKey ctxKeyType = 42

type ctxKeyType int

type ctxValues struct {
	User string
	ID   string
	// ...
	Err error
}

func buildMux(api *API) http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("GET /health", api.healthHandler)
	mux.HandleFunc("POST /tokenize", api.tokenizeHandler)

	return topMiddleware(api.log, mux)
}

func (a *API) tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	vals := getCtxValues(r.Context())
	log := a.log.With("id", vals.ID, "path", r.URL.Path)
	// Step 1: Unmarshal & Validate
	defer r.Body.Close()
	data, err := io.ReadAll(r.Body)
	if err != nil {
		log.Error("can't read", "error", err, "peer", r.RemoteAddr)
		http.Error(w, "read error", http.StatusBadRequest)
		return
	}
	if len(data) == 0 {
		log.Error("no data", "error", err, "peer", r.RemoteAddr)
		http.Error(w, "no data", http.StatusBadRequest)
		return
	}
	text := string(data)

	// Step 2: Work
	tokens := nlp.Tokenize(text)
	log.Info("tokens", "count", len(tokens))

	// Step 3: Write response
	reply := map[string]any{
		"tokens": tokens,
	}

	if err := writeJSON(w, reply); err != nil {
		a.log.Error("write json", "error", err)
	}
}

func writeJSON(w http.ResponseWriter, data any) error {
	w.Header().Set("content-type", "application/json")
	return json.NewEncoder(w).Encode(data)
}

func (a *API) healthHandler(w http.ResponseWriter, r *http.Request) {
	vals := getCtxValues(r.Context())
	log := a.log.With("id", vals.ID, "path", r.URL.Path)
	log.Info("health")
	fmt.Fprintln(w, "OK")
}

type API struct {
	log *slog.Logger
}
