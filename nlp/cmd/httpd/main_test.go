package main

import (
	"log/slog"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestHealth(t *testing.T) {
	r := httptest.NewRequest("GET", "/health", nil)
	w := httptest.NewRecorder()

	a := API{
		log: slog.Default(),
	}

	// a.healthHandler(w, r) // Bypass middleware
	mux := buildMux(&a)
	mux.ServeHTTP(w, r)

	resp := w.Result()
	require.Equal(t, http.StatusOK, resp.StatusCode)
}

/* Testing handlers
- handler as a function, httptest
	- bypass (or not) middleware
- In memory server
	- Need a "run" function instead of main
- Build server
	- t.TempDir
	- os/exec + go build
*/
