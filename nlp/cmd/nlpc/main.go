package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"log/slog"
	"os"
	"path"
	"time"

	"github.com/353solutions/nlp/pb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

/* Configuration
- Defaults: Code
- Configuration File: YAML/TOML (not in stdlib)
- Environment: os.Getenv
- Command line: flag

External:
- viper + cobra
- ardanlabs conf
- cli
- ...
*/

/* Metrics & Logging
Metrics
- expvar (built-in)
- prometheus
- otel
- ...

Logging
- log/slog (built-in)
- zap
- logrus
...
*/
// go build -ldflags='-X main.version=0.2.0' ./cmd/nlpc
var version = "0.1.2"

var options struct {
	showVersion bool
}

func main() {
	flag.BoolVar(&options.showVersion, "version", false, "show version & exit")
	flag.Parse()

	if options.showVersion {
		fmt.Printf("%s version %s\n", path.Base(os.Args[0]), version)
		os.Exit(0)
	}
	addr := "localhost:8081"
	creds := insecure.NewCredentials()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	conn, err := grpc.DialContext(
		ctx,
		addr,
		grpc.WithTransportCredentials(creds),
		grpc.WithBlock(),
	)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer conn.Close()

	slog.Info("connected", "address", addr)
	// For performance use: slog.LogAttrs
	c := pb.NewNLPClient(conn)

	req := pb.TokenizeRequest{
		Text: "Who's on first?",
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	resp, err := c.Tokenize(ctx, &req)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println(resp.Tokens)
}
