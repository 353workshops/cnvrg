package main

import (
	"context"
	"log/slog"
	"net"
	"os"

	"github.com/353solutions/nlp"
	"github.com/353solutions/nlp/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	var nlp NLP
	srv := grpc.NewServer()
	pb.RegisterNLPServer(srv, &nlp)
	reflection.Register(srv)

	addr := ":8081"
	slog.Info("server starting", "address", addr)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		slog.Error("listen", "error", err)
		os.Exit(1)
	}

	if err := srv.Serve(lis); err != nil {
		slog.Error("serve", "error", err)
		os.Exit(1)
	}
}

func (n *NLP) Tokenize(ctx context.Context, req *pb.TokenizeRequest) (*pb.TokenizeResponse, error) {
	tokens := nlp.Tokenize(req.Text)
	resp := pb.TokenizeResponse{
		Tokens: tokens,
	}
	return &resp, nil
}

type NLP struct {
	pb.UnimplementedNLPServer
}

/* gRPC = Protocol buffers over HTTP/2
response/request
streaming: client->server, server->client, server <-> client
*/

/*
function call:
a, b := 1, 2
out := add(1, 2)

RPC:
[client]
req := marshal(a, b)
network call -> /add [req]
[server]
route /add -> fn
a, b = unmarshal(req)
out = add(a, b)
resp = marshal(out)
network call -> [out]
[client]
out = unmarshal(data)
*/
