package db

import (
	"context"
	"database/sql"
	_ "embed"

	_ "modernc.org/sqlite"
)

type DB struct {
	conn *sql.DB
}

func Open(ctx context.Context, path string) (*DB, error) {
	conn, err := sql.Open("sqlite", path)
	if err != nil {
		return nil, err
	}

	if err := conn.PingContext(ctx); err != nil {
		conn.Close()
		return nil, err
	}
	// TODO: SELECT 1 FROM stop_words

	db := DB{
		conn: conn,
	}
	return &db, nil
}

func (db *DB) Close() error {
	return db.conn.Close()
}

//go:embed sql/query.sql
var isStopSQL string

func (db *DB) IsStop(ctx context.Context, word string) bool {
	// See also sql.Named to pass parameters by name
	// Not all drivers implement it
	row := db.conn.QueryRowContext(ctx, isStopSQL, word)
	var count int
	if err := row.Scan(&count); err != nil {
		return false
	}

	return count > 0
}

/*
Row: 1, "the"
var count int
var word string
row.Scan(&count, &word)
*/
