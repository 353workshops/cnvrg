package db

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	deadline := time.Now().Add(30 * time.Second)
	if d, ok := t.Deadline(); ok {
		deadline = d
	}
	ctx, cancel := context.WithDeadline(context.Background(), deadline)
	defer cancel()

	db, err := Open(ctx, "testdata/stop_words.db")
	require.NoError(t, err, "open")
	defer db.Close()

	require.True(t, db.IsStop(ctx, "can"), "can")
	require.False(t, db.IsStop(ctx, "go"), "go")
}
