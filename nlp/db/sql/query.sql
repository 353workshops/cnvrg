SELECT
    COUNT(word)
FROM
    stop_words
WHERE
    word = ?
;
