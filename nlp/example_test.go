package nlp_test

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/353solutions/nlp"
)

func ExampleTokenize() {
	text := "Who's on first?"
	tokens := nlp.Tokenize(text)
	fmt.Println(tokens)

	// Output:
	// [who on first]
}

// Show subprocess output
func Example_python() {
	cmd := exec.Command("python", "--version")
	cmd.Stdout = os.Stdout
	err := cmd.Run()
	fmt.Println(err)

	// Output:
	// Python 3.12.0
	// <nil>
}
