package nlp

import (
	"regexp"
	"strings"

	"github.com/353solutions/nlp/stemmer"
)

/* `hello` is a "raw string"
fmt.Println("c:\to\new\report.csv")
c:	o
ew

fmt.Println(`c:\to\new\report.csv`)
c:\to\new\report.csv

Usage:
1. Multi line string
2. Regular expression (\d+)
3. Windows path
*/

/*
	Code that runs before main

- var with function call
- init
*/
var (
	// "Who's on first" -> [who s on first]
	wordRe = regexp.MustCompile(`[a-zA-Z]+`)
)

// Tokenize returns list of tokens found in text.
func Tokenize(text string) []string {
	words := wordRe.FindAllString(text, -1)
	var tokens []string
	for _, w := range words {
		token := strings.ToLower(w)
		token = stemmer.Stem(token)
		if token != "" {
			tokens = append(tokens, token)
		}
	}
	return tokens
}
