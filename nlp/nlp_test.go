package nlp

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

/*
var tokenizeCases = []struct {
	name   string
	text   string
	tokens []string
}{
	{"simple", "Who's on first?", []string{"who", "s", "on", "first"}},
	{"empty", "", nil},
}
*/

type tokCase struct {
	Name   string
	Text   string
	Tokens []string
}

func loadTokenizeCases(t *testing.T) []tokCase {
	file, err := os.Open("testdata/tokenize_cases.yml")
	require.NoError(t, err, "open")
	defer file.Close()

	var cases []tokCase
	err = yaml.NewDecoder(file).Decode(&cases)
	require.NoError(t, err, "YAML")
	return cases
}

// Exercise: Use data from testdata/tokenize_cases.yml instead of tokenizeCases variable
// Hint: gopkg.in/yaml.v3
func TestTokenizeTable(t *testing.T) {
	//for _, tc := range tokenizeCases {
	for _, tc := range loadTokenizeCases(t) {
		name := tc.Name
		if name == "" {
			name = tc.Text
		}
		t.Run(name, func(t *testing.T) {
			tokens := Tokenize(tc.Text)
			/*
				if !reflect.DeepEqual(tc.tokens, tokens) {
					t.Fatalf("\nexpected: %#v\ngot     : %#v", tc.tokens, tokens)
				}
			*/
			require.Equal(t, tc.Tokens, tokens)
		})
	}
}

func TestTokenize(t *testing.T) {
	text := "What's on second?"
	tokens := Tokenize(text)
	expected := []string{"what", "on", "second"}
	/* before testify
	// if tokens != expected {
	if !reflect.DeepEqual(expected, tokens) {
		t.Fatalf("\nexpected: %#v\ngot     : %#v", expected, tokens)
	}
	*/
	require.Equal(t, expected, tokens)
}

/*
Suite: teardown, setup

function:
	- setup: call a function
	- teardown: t.Cleanup, defer
suite:
	See testify/suite
package
	- setup: TestMain
	- teardown: TestMain
all tests:
	- Outside of Go (Makefile, bash ...)
*/

/*
A -> C v1.2
B -> C v1.3
*/

func TestCI(t *testing.T) {
	// Jenkins: Use BUILD_NUMBER
	if os.Getenv("CI") == "" {
		t.Skip("not in CI")
	}
	// ...
}

/* Options for test selection
- -run REGEXP
- -short + if testing.Short() { ... }
- environment variable, os.Getenv + t.Skip()
- build tags, //go:build qa + go test -tags qa
*/

func FuzzTokenize(f *testing.F) {
	f.Add("") // empty string

	// Currently, Go can fuzz only numbers, []byte and string
	fn := func(t *testing.T, text string) {
		tokens := Tokenize(text)

		// Heuristic to check that output is not wrong
		lText := strings.ToLower(text)
		for _, tok := range tokens {
			if !strings.Contains(lText, tok) {
				t.Fatalf("unknown token: %#v", tok)
			}
		}
	}

	f.Fuzz(fn)
}

/*
type User struct {
	Login string
	Followers int
}

fn := func(t *testing.T, login string, followers int) {
	u := User{login, followers}
	...
}
*/
