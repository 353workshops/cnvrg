// RTB: Real Time Bidding
package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	// We have 50 msec to return an answer
	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancel()

	url := "https://go.dev"
	bid := bidOn(ctx, url)
	fmt.Println(bid)
}

// If algo didn't finish in time, return a default bid
func bidOn(ctx context.Context, url string) Bid {
	ch := make(chan Bid, 1) // buffered channel to avoid goroutine leak
	go func() {
		bid := bestBid(url)
		ch <- bid
	}()

	select {
	case bid := <-ch:
		return bid
	case <-ctx.Done():
		break
	}

	return defaultBid
}

var defaultBid = Bid{
	AdURL: "http://adsЯus.com/default",
	Price: 3,
}

var bbTime = 100 * time.Millisecond

// Written by Algo team, time to completion varies
func bestBid(url string) Bid {
	time.Sleep(bbTime)

	return Bid{
		AdURL: "http://adsЯus.com/ad7",
		Price: 7,
	}
}

type Bid struct {
	AdURL string
	Price int // In ¢
}
