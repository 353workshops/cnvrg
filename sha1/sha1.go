package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	// GoLand: sha1/http.log.gz
	fmt.Println(fileSHA1("http.log.gz"))
	fmt.Println(fileSHA1("sha1.go"))
	/*
		// fmt.Println(fileSHA1("sha1.go"))
		_, err := fileSHA1("sha1.go")
		for err != nil {
			fmt.Printf("> %s (%T)\n", err, err)
			err = errors.Unwrap(err)
		}
	*/
}

// Exercise: Decompress only if fileName ends with ".gz"
// Hint: strings.HasSuffix
// cat sha1.go | sha1sum
// cat http.log.gz | gunzip | sha1sum
func fileSHA1(fileName string) (string, error) {
	// cat http.log.gz
	// Idiom: open resource, check for error, defer release
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file

	if strings.HasSuffix(fileName, ".gz") {
		// | gunzip
		// r, err := gzip.NewReader(file) // BUG: r shadows r from line 39
		r, err = gzip.NewReader(file)
		if err != nil {
			// Add context to errors
			return "", fmt.Errorf("%q - %w", fileName, err)
		}
		fmt.Printf("using gzip: %p\n", r)
	}

	// | sha1sum
	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", fmt.Errorf("%q - can't copy - %w", fileName, err)
	}
	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
