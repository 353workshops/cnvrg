package main

import (
	"fmt"
	"sort"
)

func main() {
	cart := []string{"lemon", "apple", "banana"}
	fmt.Println("cart:", cart)
	cart = append(cart, "bread")
	fmt.Println("cart (append):", cart)
	//fruit := cart[:3] // slicing, half-open range
	fruit := cart[:3:3]
	fmt.Println("fruit:", fruit)
	fruit[0] = "lime"
	fmt.Println("fruit:", fruit)
	fmt.Println("cart:", cart)
	fmt.Printf("fruit: len=%d, cap=%d\n", len(fruit), cap(fruit))
	fruit = append(fruit, "avocado")
	fmt.Println("fruit:", fruit)
	fmt.Println("cart:", cart)

	var values []int
	for i := range 2000 { // go 1.22 +
		values = appendInt(values, i)
	}
	fmt.Println(values[:10])

	s1 := make([]int, 5)
	s2 := s1[1:3]
	fmt.Printf("s1: %p, s2: %p\n", &s1[0], &s2[0])

	nums := []float64{3, 1, 2, 4}
	fmt.Println(median(nums))
	fmt.Println("nums:", nums)

	bank := []Account{
		{"Donald", "regular", 1_000},
		{"McScrooge", "vip", 1_000_000},
	}
	// value semantics range (changes won't reflect in bank)
	for _, a := range bank {
		if a.Type == "vip" {
			a.Balance += 1_000
		}
	}

	// pointer semantics range
	for i := range bank {
		if bank[i].Type == "vip" {
			bank[i].Balance += 1_000
		}
	}

	// read/modify/write (transaction)
	for i, a := range bank {
		if a.Type == "vip" {
			a.Balance += 1_000
			bank[i] = a
		}
	}

	for _, a := range bank {
		fmt.Println(a)
	}
}

type Account struct {
	Name    string
	Type    string
	Balance int
}

/*
- median of empty slice is an error (fmt.Errorf)
- sort values (sort.Float64s)
- if odd number of elements - return middle
- return average of two middle
*/
func median(nums []float64) (float64, error) {
	if len(nums) == 0 {
		return 0, fmt.Errorf("median of empty slice")
	}

	// Copy in order not to change original nums
	values := make([]float64, len(nums))
	copy(values, nums)

	sort.Float64s(values)
	i := len(values) / 2
	if len(values)%2 == 0 {
		return (values[i-1] + values[i]) / 2.0, nil
	}
	return values[i], nil
}

func appendInt(values []int, v int) []int {
	if len(values) == cap(values) { // underlying array is full, allocate + copy
		size := 2 * (len(values) + 1)
		fmt.Println(len(values), "->", size)
		vs := make([]int, size)
		copy(vs, values)
		values = vs[:len(values)]
	}

	values = values[:len(values)+1]
	values[len(values)-1] = v
	return values
}
